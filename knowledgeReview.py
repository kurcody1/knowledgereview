# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 12:13:17 2019

@author: Faye
"""
import numpy as np
import matplotlib.pyplot as plt
import laModule as la


# %% 3a
# =======================Setting up and plotting data==========================
print("dataset 1\n")
X = np.zeros((100, 2))
X[:50, :] = np.random.normal([10, 0], [1.2, 1.2], [50, 2])
X[50:, :] = np.random.normal([-10, 0], [1.2, 1.2], [50, 2])
n = len(X[0, :])
m = len(X[:, 0])

fig1 = plt.figure(1, (10, 5))
fig1.suptitle("Dataset 1")
plt.subplot(121)
plt.scatter(X[:, 0], X[:, 1], marker='o')
plt.xlim(-15, 15)
plt.ylim(-15, 15)
plt.xlabel('x1')
plt.ylabel('x2')

XNew1 = np.zeros((50, 2))
for i in range(n):
    mean = sum(X[:50, i] / 50)
    XNew1[:, i] = X[:50, i] - mean

covariance1 = (XNew1.T.dot(XNew1)) / m
covariance2 = 1 / (m - 1) * (X[50:, :].T.dot(X[50:, :]))
# print("Covariance Matricies:\n", covariance1, "\n", covariance2)

# =============================Applying PCA====================================

# center data
mean = np.zeros(n)
for i in range(n):
    mean = sum(X[:, i]) / m
    X[:, i] = X[:, i] - mean
covariance = 1 / (m - 1) * (X.T.dot(X))
vals, principalComponents = np.linalg.eig(covariance)
print(f"vals {vals}") #\nprincipalComponents{principalComponents}")

# 2 principal components
X_new = np.zeros((m, n))
for i in range(m):
    X_new[i, :] = la.project(X[i, :], principalComponents)

plt.scatter(X_new[:, 0], X_new[:, 1], marker='.')
plt.xlim(-15, 15)
plt.ylim(-15, 15)

# 1 principal component
X_new = np.zeros((m, n))
for i in range(m):
    X_new[i, :] = la.project(X[i, :], principalComponents[0, :])

plt.subplot(122)
plt.scatter(X_new[:, 0], X_new[:, 1], marker='.')
plt.xlim(-15, 15)
plt.ylim(-15, 15)

print("pca seemed to work well, very little variance came from the second",
      "principal component. If we were trying to classify 2 categories, the",
      "second component was not useful.")
# %% 3b
print("\nDataset 2")

X = np.zeros((100, 2))
X[:50, :] = np.random.multivariate_normal([5, -3], [[.5, 0], [0, .6]], 50)
X[50:, :] = np.random.multivariate_normal([-5, 3], [[1.7, 0], [0, 1.5]], 50)
n = len(X[0, :])
m = len(X[:, 0])

# find covariance of first 50 points
Xcov = np.zeros((50, 2))
for i in range(n):
    mean = sum(X[:50, i]) / 50
    Xcov[:, i] = X[:50, i] - mean

print("covariance of first 50 points when I use X.T.dot(X)",
      Xcov.T.dot(Xcov) / 50)


# plot X
fig2 = plt.figure(2, (10 ,5))
fig2.suptitle("Dataset 2")
plt.subplot(121)
plt.scatter(X[:, 0], X[:, 1])
plt.xlim(-15, 15)
plt.ylim(-15, 15)

eigenvalues, principalComponents, covarianceMatrix, ceneteredX = la.PCA(X)

# plot X projected onto the first 2 principal components
XProjected = np.zeros((m, n))
for i in range(m):
    XProjected[i, :] = la.project(X[i, :], principalComponents)

plt.scatter(XProjected[:, 0], XProjected[:, 1], marker='.')
plt.xlim(-15, 15)
plt.ylim(-15, 15)

# plot X projected onto the first principal component
plt.subplot(122)
for i in range(m):
    XProjected[i, :] = la.project(X[i, :], principalComponents[:, 0])

plt.scatter(XProjected[:, 0], XProjected[:, 1])
plt.xlim(-15, 15)
plt.ylim(-15, 15)
print(f"eigenvalues: {eigenvalues}")

print("again, PCA was successfull in removing the variance in the component",
      " that was not important to us and classification will work as well.")

# %% 3c
print("\nDataset 3")

X = np.zeros((100, 2))
X[:50, :] = np.random.multivariate_normal([1, 0], [[.2, 0], [0, 5.2]], 50)
X[50:, :] = np.random.multivariate_normal([-1, 0], [[.2, 0], [0, 5.2]], 50)

fig3 = plt.figure(3, (10, 5))
fig3.suptitle("Dataset 3")
plt.subplot(121)
plt.scatter(X[:, 0], X[:, 1])

values, principalComponents, covarianceMatrix, XCentered = la.PCA(X)

XNew = np.zeros((m, n))
for i in range(m):
    XNew[i, :] = la.project(X[i, :], principalComponents)
plt.scatter(XNew[:, 0], XNew[:, 1], marker='.')

plt.subplot(122)
for i in range(m):
    XNew[i,:] = la.project(X[i, :], principalComponents[1, :])
plt.scatter(XNew[:, 0], XNew[:, 1])

print(f"eigenvalues{values} CovarianceMatrix: {covarianceMatrix}")
varianceRetained = values[1] / (values[1] + values[0]) * 100
print(f"only {varianceRetained}% of the variance is retained")
print("With these datasets, PCA works well when the means of the classes in",
      " at least 1 dimension are far appart. In general, PCA is useful when ",
      "the variance is very small in some dimensions compared to the others")
