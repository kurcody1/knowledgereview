# -*- coding: utf-8 -*-
"""
Created on Sun Jul 14 11:13:12 2019

@author: Faye
"""

import numpy as np


def project(x, A):
    '''
    columns of the matrix A form basis for the subspace to project on.
    x is the vector being projected onto the subspace
    '''
    # check if A has more than one vector. Different method of calculation for
    # 1D projection (error: pinv)
    if len(A.shape) != 1:
        np.asarray(A)
        A_TAInverse = np.linalg.pinv(A.T.dot(A))
        projectedVector = A.dot(A_TAInverse).dot(A.T).dot(x)
        return projectedVector
    else:
        projectedVector = (sum(x * A) / sum(A * A)) * A
        return projectedVector


def PCA(X):
    n = len(X[0, :])
    m = len(X[:, 0])
    centeredX = np.zeros((m, n))
    # Center data
    for j in range (n):
        featureMean = sum(X[:, j]) / m
        centeredX[:, j] = X[:, j] - featureMean
    # make covarianceMatrix to find principal components
    covarianceMatrix = X.T.dot(X)
    eigenvalues, principalComponents = np.linalg.eig(covarianceMatrix)
    return eigenvalues, principalComponents, covarianceMatrix, centeredX
